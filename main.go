package main

import (
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	vaultURL := os.Getenv("VAULT_URL")
	vaultToken := os.Getenv("VAULT_TOKEN")
	sigs := make(chan os.Signal, 1)
	stop := make(chan struct{})
	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	wg := &sync.WaitGroup{}

	clientset, err := newClientSet(false)

	if err != nil {
		panic(err.Error())
	}

	dockerConfigString := getVaultSecret(vaultURL, vaultToken)
	client := newSecretsController(clientset, dockerConfigString)
	go client.Run(stop, wg)

	<-sigs
	log.Println("Bye")
	close(stop)
	wg.Wait()
}
