FROM golang:latest AS builder

WORKDIR /go/src/gitlab.com/Mrakie/test-proj
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /secretcontroller

FROM alpine:3.10
WORKDIR /app
COPY --from=builder /secretcontroller /app/secretcontroller
ENTRYPOINT ["/app/secretcontroller"]
