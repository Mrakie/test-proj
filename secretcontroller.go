package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

// SecretsController watches the kubernetes api
type SecretsController struct {
	namespaceInformer cache.SharedIndexInformer
	kclient           *kubernetes.Clientset
	Secret            string
}

// Run start schedule
func (c *SecretsController) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()

	wg.Add(1)

	go c.namespaceInformer.Run(stopCh)

	<-stopCh
}

func (c *SecretsController) createCredSecret(obj interface{}) {
	namespaceObj := obj.(*v1.Namespace)
	namespaceName := namespaceObj.Name

	data := map[string]string{
		".dockerconfigjson": c.Secret,
	}

	secret := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: "zona-registry",
		},
		StringData: data,
		Type:       v1.SecretTypeDockerConfigJson,
	}
	_, err := c.kclient.CoreV1().Secrets(namespaceName).Create(secret)

	if err != nil {
		log.Println(fmt.Sprintf("Failed to create Secret: %s, in %s Namespace.", err.Error(), namespaceName))
	} else {
		log.Println(fmt.Sprintf("Created Secret for Namespace: %s", namespaceName))
	}
}

func newSecretsController(kclient *kubernetes.Clientset, secretString string) *SecretsController {
	secretsWatcher := &SecretsController{}
	namespaceInformer := cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options metav1.ListOptions) (runtime.Object, error) {
				return kclient.CoreV1().Namespaces().List(options)
			},
			WatchFunc: func(options metav1.ListOptions) (watch.Interface, error) {
				return kclient.CoreV1().Namespaces().Watch(options)
			},
		},
		&v1.Namespace{},
		15*time.Minute,
		cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc},
	)

	namespaceInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: secretsWatcher.createCredSecret,
	})

	secretsWatcher.kclient = kclient
	secretsWatcher.namespaceInformer = namespaceInformer
	secretsWatcher.Secret = secretString
	return secretsWatcher
}
