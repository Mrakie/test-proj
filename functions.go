package main

import (
	"log"
	"os"
	"path/filepath"

	"github.com/hashicorp/vault/api"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func newClientSet(runOutsideCluster bool) (*kubernetes.Clientset, error) {
	kubeConfigLocation := ""

	if runOutsideCluster == true {
		homeDir := os.Getenv("HOME")
		kubeConfigLocation = filepath.Join(homeDir, ".kube", "config")
	}

	config, err := clientcmd.BuildConfigFromFlags("", kubeConfigLocation)

	if err != nil {
		return nil, err
	}

	return kubernetes.NewForConfig(config)
}

func getVaultSecret(url string, token string) string {
	keyName := "zona-registry/data/common"
	client, err := api.NewClient(&api.Config{
		Address: url,
	})
	if err != nil {
		log.Fatal(err)
	}
	client.SetToken(token)
	secretValues, err := client.Logical().Read(keyName)
	if err != nil {
		log.Fatal(err)
	}

	m := secretValues.Data["data"].(map[string]interface{})["dockerconfigjson"].(string)
	return m
}
